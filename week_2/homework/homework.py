###============================
# function : detect number is mode a || mode b || mode a&b
# params : number, a , strA, b , strB
# return : none
###============================

def detect(num, a, strA, b, strB):
	if num <= 1 or a < 0 or b < 0:
		print('invalid input params')
		return

	for i in range(1, num + 1):
		### 1st way
		# ret = i
		# if(i % a == 0 and i % b == 0):
		# 	ret = strA + strB
		# elif(i % a == 0):
		# 	ret = strA
		# elif(i % b == 0):
		# 	ret = strB

		### 2nd way
		ret = strA + strB if i % a == 0 and i % b == 0 \
			else strA if i % a == 0 \
			else strB if i % b == 0 \
			else i

		print('{}'.format(ret))

num = 10
a = 3
strA = 'fizz'
b = 5
strB = 'buzz'
detect(num, a, strA, b, strB)

###============================
# function : unzip a list of list
# params : list of list (vector2 --> vector1)
# return : none
###============================

def unzip(lofl):
	if(type(lofl) is not list):
		print('invalid input params')
		return

	ret = []
	for i in range(len(lofl)):
		ret = ret + lofl[i]
	print('[1st] unzip lofl {}'.format(ret))

a = [[1, 4], [9, 16], [25, 36]]
unzip(a)

# 2nd way by one line
b = list(a[0] + a[1] + a[2])
print('[2nd] unzip lofl {}'.format(b))

# 3rd way (from solution)
b = [item for subList in a for item in subList]
print('[3rd] unzip lofl {}'.format(b))

###============================
# function : print list of dict[key] sort by dict[value]
# params : dictionary
# return : none
###============================

myDict = {'a': 9, 'b': 1, 'c': 12, 'd': 7}

def sortFunc(key):
	return myDict[key]

def sortDictByValue():
	if(type(myDict) is not dict):
		print('invalid input params')
		return

	ret = list(myDict.keys())
	ret.sort(key=sortFunc)
	print('1st way sort dict by key {}'.format(ret))

sortDictByValue()

# 2nd way by one line (from solution)
# a = [item[0] for item in sorted(list(myDict.keys()), key = lambda x: x[1])]
# print('2nd way sort {}'.format(a))
# --> do not understand and not correct

# 3rd way by one line
a = sorted(list(myDict.keys()), key=lambda key: myDict[key])
print('2nd way sort dict by key {}'.format(a))

###============================
# function : make list of common values from 2 input lists
# params : 2 input lists
# return : none
###============================

### check speed test
import time

def speed_test(func):
    def wrapper(*args, **kwargs):
        t1 = time.time()
        # for x in range(5000):
        results = func(*args, **kwargs)
        t2 = time.time()
        print('{} took %0.3f ms {}'.format(func, (t2 - t1) * 1000.0))
        return results
    return wrapper

# 1st way
@speed_test
def compare_mergeFor(listA, listB):
	if (type(listA) is not list or type(listB) is not list):
		print('invalid input params')
		return

	ret = [x for x in listA if x in listB]

	# print('compare_mergeFor {}'.format(ret))
	return ret

# 2nd way
@speed_test
def compare_mergeDict(listA, listB):
	if(type(listA) is not list or type(listB) is not list):
		print('invalid input params')
		return

	dictA = {}
	for i in range(len(listA)):
		dictA[listA[i]] = dictA.get(listA[i], 0) + 1

	dictB = {}
	for i in range(len(listB)):
		dictB[listB[i]] = dictB.get(listB[i], 0) + 1

	ret = []
	for key in dictA.keys():
		if(key in dictB):
			ret.append(key)

	# print('compare_mergeList {}'.format(ret))
	return ret

# 3rd way
@speed_test
def compare_mergeSet(listA, listB):
	if (type(listA) is not list or type(listB) is not list):
		print('invalid input params')
		return

	set_A = frozenset(listA)
	set_B = frozenset(listB)
	ret = set_A & set_B

	# print('compare_mergeSet {}'.format(ret))
	return ret

# 4rd way
@speed_test
def compare_mergeIntersect(listA, listB):
	if (type(listA) is not list or type(listB) is not list):
		print('invalid input params')
		return

	ret = frozenset(listA).intersection(listB)

	# print('compare_mergeIntersect {}'.format(ret))
	return ret

# compare short lists
print('1st compare short lists')
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
compare_mergeFor(a, b)
compare_mergeDict(a, b)
compare_mergeSet(a, b)
compare_mergeIntersect(a, b)

# compare long lists
# print('2nd compare long lists')
# import random
# a = random.sample(range(100000), 100000)
# b = random.sample(range(150000), 150000)
# compare_mergeFor(a, b)
# compare_mergeDict(a, b)
# compare_mergeSet(a, b)
# compare_mergeIntersect(a, b)


###============================
# function : check input string is palindrome
# params : input string
# return : none
###============================

def checkPalindrome(inputStr):
	if(type(inputStr) is not str):
		print('invalid input params')
		return

	inputList = list(inputStr)
	inputList.reverse()
	reverseStr = ''.join(inputList)

	isPalindrome = inputStr == reverseStr
	print('{}'.format(isPalindrome))

a = 'abcdcba'
b = '123421'
checkPalindrome(a)
checkPalindrome(b)

# 2nd way (from solution)
def is_palindrome_interactive(inputStr):
	for i in range(len(inputStr) // 2):
		if inputStr[i] == inputStr[-1 - i]:
			return False
	return True

# 3rd way (from solution)
def is_palindrome_recursive(inputStr):
	if len(inputStr) <= 1:
		return True
	else:
		return (inputStr[0] == inputStr[-1]) and is_palindrome_recursive(inputStr[1:-1])

is_palindrome_interactive(a)
is_palindrome_recursive(a)

###============================
# function : wrap string with Width 
# params : input string
# return : none
###============================

#1st (from solution)
def print_paragraph(inputStr, width):
	if (type(inputStr) is not str) and (type(width) is not num):
		print('invalid input params')
		return
	for i in range(0, len(inputStr) + 1, width):
		print('{}'.format(inputStr[i: width + i]))


inputStr = 'abcdefghijklmnopq'
width = 5
print_paragraph(inputStr, width)


