### lession summary
# 1. create nparrays [ use np.array() ]
# 2. some init arrays methods [ .zeros() || .ones() || .arange() || ... ]
# 3. arithmetic [ + || - || * || / || ** || ... ]
# 4. index and slicing [ by index || row & column ]
# -> note : slice will modified on original view -> if not want use [ .copy() ]
# 5. indexing with slices
# 6. comparision
# 7. boolean & boolean indexing
# 8. fancy indexing
# -> can combine fancy with other indexing methods
# 9. universal function [ use { %time } to track time computing ]
# 10.


import numpy as np

arrSize = 10

# 1. negative all elements from [3, 8] inplace
arr = np.arange(arrSize)
print('[ exe_1 ] arr before : {}'.format(arr))
arr[(arr >= 3) & (arr <= 8)] *= -1
print('[ exe_1 ] arr after : {}'.format(arr))

# 2. replace max element in array to 0
arr = np.random.randint(0, arrSize - 1, arrSize)
print('[ exe_2 ] arr before : {}'.format(arr))
arr[arr.argmax()] = -1
print('[ exe_2 ] arr after : {}'.format(arr))

# 3. find common values of 2 arrays
arr_1 = np.random.randint(0, arrSize - 1, arrSize)
arr_2 = np.random.randint(0, arrSize - 1, arrSize)
print('[ exe_3 ] arr_1 before : {}'.format(arr_1))
print('[ exe_3 ] arr_2 before : {}'.format(arr_2))
s3 = np.intersect1d(arr_1, arr_2)
print('[ exe_3 ] common values : {}'.format(s3))

# 4. reverse a vector
arr = np.random.randint(0, arrSize - 1, arrSize)
print('[ exe_4 ] arr before : {}'.format(arr))
arr = arr[::-1]
print('[ exe_4 ] arr after : {}'.format(arr))

# 5. create matrix 3x3 with values range from [0, 8]
arr = np.arange(arrSize - 1).reshape(3, 3)
print('[ exe_5 ] arr before : {}'.format(arr))

# 6. find index of non-zero elements from arr
arr = np.array([1, 2, 0, 0, 4, 0])
print('[ exe_6 ] arr before : {}'.format(arr))
s6 = np.nonzero(arr)
print('[ exe_6 ] arr index : {}'.format(s6))

# 7. create 3x3x3 array with random value
arr = np.random.randint(0, arrSize - 1, (3, 3, 3))
print('[ exe_7 ] arr before : {}'.format(arr))

# 8. create random arr with size = 30 and find mean value
arr = np.random.randint(0, arrSize, 30)
print('[ exe_8 ] arr before : {}'.format(arr))
print('[ exe_8 ] mean value : {}'.format(np.mean(arr)))

# 9. create 2d array with 1 border and 0 inside
arr = np.ones((5, 5))
print('[ exe_9 ] arr before : {}'.format(arr))
arr[1:-1, 1:-1] = 0
print('[ exe_9 ] arr after : {}'.format(arr))

# 10. find closest index
arr_1 = np.random.randint(0, 100, 20)
print('[ exe_10 ] arr_1 before : {}'.format(arr_1))
arr_2 = np.random.uniform(0, 20)
print('[ exe_10 ] arr_2 before : {}'.format(arr_2))
closestIdx = np.argmin(abs(arr_1 - arr_2))
print('[ exe_10 ] closest index is {} with value {}'.format(closestIdx, arr_1[closestIdx]))

