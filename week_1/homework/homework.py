###=================
# print current system time
###=================
import datetime as dt

now = dt.datetime.now()
print('Current date and time : {} '.format(now))
# format obj [now] to string date time (exclude mili second)
print('current date with format {} '.format(now.strftime('%m/%d/%Y %H::%M::%S')))

#===========================================================#

###=================
# sum function
# params : x, y
# return : (x + y) in range(15,20) ? 20 : (x + y)
###=================
def sum(x, y):
	sum = x + y
	if sum in range(15, 20):
		return 20
	else:
		return sum

print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))



